import React, {useState, useEffect} from "react";
import 'bootstrap/dist/css/bootstrap.css';
import axios from "axios";
import {Link} from "react-router-dom"

function Datatable () {
    const [users, setUsers] = useState([]);

    useEffect(() => {
        loadData();
    }, []);

    const loadData = async () => {
        const result = await axios.get("http://localhost:3001/users");
        setUsers(result.data);
    }

    return (
        <div className="text-center">
            <table className="table table-bordered table-hover">
                <thead className="thead-dark">
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Website</th>
                        <th>Edit</th>
                    </tr>
                </thead>
                <tbody>
                    {users.map((user,index) => (
                        <tr>
                            <td>{user.id}</td>
                            <td>{user.name}</td>
                            <td>{user.username}</td>
                            <td>{user.email}</td>                                     
                            <td>{user.phone}</td>
                            <td>{user.website}</td>            
                            <td><Link className="btn btn-outline-primary" to={`/edit/editdata/${user.id}`}>Edit</Link></td>
                        </tr>
                    ))}
                    
                </tbody>
            </table>
        </div>       
    );
}

export default Datatable;