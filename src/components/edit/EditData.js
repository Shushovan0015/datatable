import React, { useState,useEffect } from "react";
import axios from "axios";
import {useHistory,useParams} from "react-router-dom";

const EditData = () => {
    const {id} = useParams();
    let history = useHistory();
    const [edit,setEdit] = useState({
        id: "",
        name:"",
        username:"",
        email:"",
        phone:"",
        website:""
    });
    
    const { name,username,email,phone,website } = edit;
    
    const onInputChange = e => {
        setEdit({...edit, [e.target.name] : e.target.value })
    }

    const onSubmit = async (e) => {
        e.preventDefault();
        await axios.put(`http://localhost:3001/users/${id}`,edit);
        history.push("/");
    }

    useEffect(() => {
        loadData();
    }, []);

    const loadData = async () => {
        const result = await axios.get(`http://localhost:3001/users/${id}` );
        setEdit(result.data);
    }

    return (
        <div className="container">
            <h1 className="text-center">Edit Data</h1>
            <form onSubmit={e => onSubmit(e)}>
                <div className="form-group">
                    <input type="text" className="form-control" placeholder="Enter Id" name="id" value={id} onChange={e => onInputChange(e)} />
                </div>
                <div className="form-group">
                    <input type="text" className="form-control"  placeholder="Enter Name" name="name" value={name} onChange={e => onInputChange(e)} />
                </div>
                <div className="form-group">
                    <input type="text" className="form-control" placeholder="Enter Username" name="lastName" value={username} onChange={e => onInputChange(e)} />
                </div>
                <div className="form-group">
                    <input type="text" className="form-control"  placeholder="Enter Email Address" name="email" value={email} onChange={e => onInputChange(e)} />
                </div>
                <div className="form-group">
                    <input type="text" className="form-control"  placeholder="Enter Phone number" name="phone" value={phone} onChange={e => onInputChange(e)} />
                </div>
                <div className="form-group">
                    <input type="text" className="form-control"  placeholder="Enter website" name="website" value={website} onChange={e => onInputChange(e)} />
                </div>
                <button className="btn btn-primary btn-block">Edit Data</button>
            </form>
        </div>
        
    );
}

export default EditData;