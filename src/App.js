import React from "react";
import Datatable from "./components/datatable/index.js"
import { BrowserRouter as Router, Switch, Route} from "react-router-dom";
import EditData from "./components/edit/EditData";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Datatable} />
        <Route exact path="/edit/editdata/:id" component={EditData} />
      </Switch>
    </Router>
  );
}

export default App;
